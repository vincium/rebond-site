<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Own\Forms\App;

use Own\Models\App\Article;

class ArticleForm extends \Generated\Forms\App\BaseArticleForm
{
    /**
     * @param Article $model = null
     * @param string $unique
     */
    public function __construct($model = null, $unique = '')
    {
        parent::__construct($model, $unique);
        $this->initApp();
    }

    public function initApp()
    {
    }
}

CREATE TABLE IF NOT EXISTS `app_article` (
    app_id INT UNSIGNED NOT NULL AUTO_INCREMENT 
,    media_id INT UNSIGNED NOT NULL
,    `description` TEXT COLLATE utf8_unicode_ci NOT NULL
    , PRIMARY KEY (app_id)
 ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

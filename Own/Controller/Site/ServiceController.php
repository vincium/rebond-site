<?php
namespace Own\Controller\Site;

use Rebond\App;

class ServiceController extends BaseController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->app->setAjax(true);
        $this->signedUser = $this->app->getUser();
    }
}


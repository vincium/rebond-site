<?php
namespace Own\Controller\Site;

use Rebond\Services\Lang;
use Rebond\Services\Template;

class HomeController extends BaseController
{
    public function setTpl()
    {
        parent::setBaseTpl();
        $this->tplMaster->set('title', Lang::lang('home'));
    }

    public function index()
    {
        // view
        $this->setTpl();

        // main
        $tplMain = new Template(Template::SITE, ['www']);

        // layout
        $this->tplLayout->set('column1', $tplMain->render('index'));

        // template
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));

        return $this->tplMaster->render('tpl-default');
    }
}

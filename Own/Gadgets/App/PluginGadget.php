<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Own\Gadgets\App;

use Rebond\App;
use Rebond\Gadgets\AbstractGadget;
use Rebond\Repository\Cms\PageRepository;
use Rebond\Services\Cms\PageService;
use Rebond\Services\Template;

class PluginGadget extends AbstractGadget
{
    public function __construct(App $app)
    {
        parent::__construct($app, 'Plugin');
    }

    public function sitemap()
    {
        $nav = PageRepository::buildSitemapNav();
        $sitemap = PageService::renderNav($nav, $this->app->url());

        $tpl = new Template(Template::MODULE, ['app', 'plugin']);
        $tpl->set('sitemap', $sitemap);
        return $tpl->render('sitemap');
    }

    public function locale()
    {
        $tpl = new Template(Template::MODULE, ['app', 'plugin']);
        $tpl->set('langs', $this->app->getLangList());
        return $tpl->render('locale');
    }

    public function landingPage()
    {
        $tpl = new Template(Template::MODULE, ['app', 'plugin']);

        $page = PageRepository::loadByUrl($this->app->url());

        $nav = PageRepository::buildSideNav($page->getId(), 0);
        $tpl->set('nav', PageService::renderNav($nav, $page->getFullUrl()));
        return $tpl->render('landing-page');
    }

    public function googleMap()
    {
        $tpl = new Template(Template::MODULE, ['app', 'plugin']);
        $tpl->set('address', $this->app->getConfig('setting-googleMap'));
        return $tpl->render('google-map');
    }
}
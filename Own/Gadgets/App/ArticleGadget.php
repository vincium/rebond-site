<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Own\Gadgets\App;

use Rebond\App;
use Own\Forms\App\ArticleForm;
use Own\Models\App\Article;
use Own\Repository\App\ArticleRepository;
use Rebond\Enums\Core\Result;
use Rebond\Gadgets\AbstractGadget;
use Rebond\Repository\Core\MediaRepository;
use Rebond\Services\Template;

class ArticleGadget extends AbstractGadget
{
    public function __construct(App $app)
    {
        parent::__construct($app, 'Article');
    }

    public function cards()
    {
        $items = ArticleRepository::loadByVersion('published');
        return $this->renderCards($items);
    }

    public function filteredCards($filterId)
    {
        $options = [];
        if ($filterId != 0) {
            $options['where'][] = ['content.filter_id = ?', $filterId];
        }
        $items = ArticleRepository::loadByVersion('published', true, $options);
        return $this->renderCards($items);
    }

    public function single($contentGroup)
    {
        $item = ArticleRepository::loadCurrent($contentGroup);
        return $this->renderSingle($contentGroup, $item);
    }

    public function editor()
    {
        $model = new Article();
        $form = new ArticleForm($model);
        return $this->renderEditor($form);
    }

}
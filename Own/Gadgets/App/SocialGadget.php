<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Own\Gadgets\App;

use Rebond\App;
use Rebond\Gadgets\AbstractGadget;
use Rebond\Services\Template;

class SocialGadget extends AbstractGadget
{
    public function __construct(App $app)
    {
        parent::__construct($app, 'Social');
    }

    public function twitter()
    {
        $twitter = $this->app->getConfig('setting-twitter');
        if (!isset($twitter)) {
            $twitter = 'vincium';
        }
        $tpl = new Template(Template::MODULE, ['app', 'social']);
        $tpl->set('twitter', $twitter);
        return $tpl->render('twitter');
    }

    public function facebookPage()
    {
        $fbPage = $this->app->getConfig('setting-facebook');
        if (!isset($fbPage)) {
            $fbPage = 'vincium';
        }
        $tpl = new Template(Template::MODULE, ['app', 'social']);
        $tpl->set('fbPage', $fbPage);
        return $tpl->render('facebook-page');
    }
}
<?php
require '../vendor/autoload.php';

use Rebond\App;
use Rebond\Enums\Core\AppLevel;
use Rebond\Controller\Admin\InstallController;

$app = App::create(AppLevel::INSTALL);
$app->launch();

$controller = new InstallController($app);
echo $controller->index();

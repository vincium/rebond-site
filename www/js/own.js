(function(Own) {
    Own.init = {
        base: function() {
            R.noti.init();
            R.modal.init();
        },
        profile: function() {
        }
    };

    Own.service = {
    };

    Own.editor = {
    };

    Own.tools = {
    };

})(window.Own = window.Own || {});

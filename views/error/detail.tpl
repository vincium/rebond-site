<div class="bg-white">
    <h2><?php echo '#' . $log['code'] . ' ' . \Rebond\Enums\Core\Code::lang($log['code']) ?></h2>
    <p><?php echo $log['message'] ?></p>
    <p><b>File</b>: <?php echo $log['file'] ?></p>
    <p><b>Line</b>: <?php echo $log['line'] ?></p>
    <p>
        <a href="/"><?php echo $this->lang('back_home') ?></a> |
        <a href="<?php echo $currentUrl ?>"><?php echo $this->lang('reload') ?></a> |
        <a href="<?php echo $adminUrl ?>tools/logs" target="_blank"><?php echo $this->lang('log_check') ?></a>
    </p>
</div>

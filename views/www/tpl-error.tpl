<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?php echo $title ?> - <?php echo $site ?></title>
    <link rel="apple-touch-icon" href="/images/brand-touch.png"/>
    <?php $this->renderMeta() ?>
    <?php $this->renderCss() ?>
</head>
<body class="body-small body-error">
    <?php echo $layout ?>
</body>
</html>
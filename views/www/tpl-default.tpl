<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?php echo $title ?> - <?php echo $site ?></title>
    <link rel="apple-touch-icon" href="/images/brand-touch.png"/>
    <?php $this->renderMeta() ?>
    <?php $this->renderCss() ?>
</head>
<body class="<?php echo $this->e($bodyClass) ?>">
    <?php echo $notifications ?>
    <div class="rb-header">
        <div class="right">
            <?php if ($signedUser->getId() != 0) { ?>
            <span><?php echo $this->lang('hi') ?>, <?php echo $signedUser->getFirstName() ?>!</span>
            <a href="#" id="rb-profile" class="modal-link"
                data-title="<?php echo $signedUser->getFullName() ?>"
                data-modal="profile"
                data-class="rb-profile-bloc"
            >
                <img src="<?php echo $this->showFromModel($signedUser->getAvatar(), 42, 42) ?>" alt="profile"/>
            </a>
            <?php } else { ?>
            <a href="/profile/sign-in"><?php echo $this->lang('sign_in') ?></a>
            <?php } ?>
        </div>
        <div id="modal-profile" class="modal">
            <img src="<?php echo $this->showFromModel($signedUser->getAvatar(), 120, 120) ?>" alt="profile"/>
            <div class="option">
                <?php if ($signedUser->getIsAdmin()) { ?>
                <a href="<?php echo $adminUrl ?>" target="_blank"><?php echo $this->lang('admin') ?></a>
                <?php } ?>
                <a href="/profile"><?php echo $this->lang('profile_my') ?></a>
                <a href="/profile/change-password"><?php echo $this->lang('password_change') ?></a>
                <form action="/profile/sign-out" method="POST" name="form" id="form">
                    <input type="submit" class="rb-link" name="sign-out" value="<?php echo $this->lang('sign_out') ?>">
                </form>
            </div>
        </div>
    </div>
    <?php echo $layout ?>
    <?php $this->debug() ?>
    <?php $this->renderJs() ?>
    <?php $this->e($footer) ?>
</body>
</html>
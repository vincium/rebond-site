<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php echo $title ?> - <?php echo $site ?></title>
</head>
<body style="font-family:Arial, Verdana, sans-serif; margin: 0; padding: 0;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #CCCCCC;">
            <tr>
                <td width="120" style="padding:10px 20px 10px 20px;">
                <img src="<?php echo $url ?>images/brand-logo.png" alt="<?php echo $title . $site ?>" title="<?php echo $title . $site ?>" width="100" height="100" />
                </td>
                <td style="color:#336699; font-size:2.5em;"><?php echo $title ?></td>
            </tr>
                <tr>
                <td colspan="2" style="padding:10px 20px 10px 20px;">
                    <?php $this->e($layout) ?>
                    <p align="right"><?php echo $this->lang('email_signature', [$site]) ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="2" bgcolor="#336699" style="padding:10px 20px 10px 20px;">
                    <a href="<?php echo $url ?>" style="color:#FFFFFF;"><?php echo $this->lang('site_view') ?></a>
                </td>
            </tr>
            </table>
        </td>
    </tr>
    </table>
</body>
</html>
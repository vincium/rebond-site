<?php $this->e($navHeader) ?>
<div class="rb-container">
    <?php $this->e($navSide) ?>
    <div id="rb-layout">
        <?php $this->e($breadcrumb) ?>
        <div class="rb-row">
            <div class="rb-col">
                <?php $this->e($column1) ?>
            </div>
        </div>
    </div>
</div>
<?php $this->e($navFooter) ?>

<div class="bg-white">
    <?php foreach ($items as $item) { ?>
    <h2>
        <a href="#" class="modal-link"
           data-modal="Article-<?php echo $item->getId() ?>"
           data-title="<?php echo $item->getTitle() ?>">
            <?php echo $item->getTitle() ?>
        </a>
        <?php if ($filter) { ?>
        <small><?php echo $item->getFilter() ?></small>
        <?php } ?>
    </h2>

    <img src="<?php echo $this->showFromModel($item->getMedia()) ?>" alt="<?php echo $item->getMedia()->getTitle() ?>" /><br>
<?php echo $item->getDescription() ?><br>

    <div class="right">
        <a href="#" class="modal-link"
           data-modal="Article-<?php echo $item->getId() ?>"
           data-title="<?php echo $item->getTitle() ?>"><?php echo $this->lang('view_more') ?></a>
    </div>
    <div class="modal" id="modal-Article-<?php echo $item->getId() ?>">
        <img src="<?php echo $this->showFromModel($item->getMedia()) ?>" alt="<?php echo $item->getMedia()->getTitle() ?>" /><br>
<?php echo $item->getDescription() ?><br>
    </div>
    <?php } ?>
</div>
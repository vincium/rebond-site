<div class="bg-white">
    <h2><?php echo $this->lang('register_success') ?></h2>
    <p><?php echo $this->lang('register_success_text') ?></p>
    <a class="rb-btn" href="/profile/sign-in"><?php echo $this->lang('sign_in') ?></a>
</div>

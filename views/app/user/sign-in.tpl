<div class="bg-white">
    <h2><?php echo $this->lang('sign_in') ?></h2>
    <form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form" class="editor">
        <?php echo $item->buildId() ?>
        <?php echo $item->buildToken() ?>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('email') ?>
                <?php echo $item->req('email') ?>
                <?php echo $item->buildEmail() ?>
            </label>
            <?php echo $item->getFieldError('email') ?>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('password') ?>
                <?php echo $item->req('password') ?>
                <?php echo $item->buildPassword() ?>
            </label>
            <?php echo $item->getFieldError('password') ?>
        </div>
        <div class="rb-form-item">
            <?php echo \Rebond\Services\Form::buildBoolean('persistentCookie', 'sign_in_remember', false) ?>
        </div>
        <div class="rb-form-item">
            <?php echo $this->buildSubmit(0, 'sign_in') ?>
            <?php echo $item->getFieldError('token') ?>
        </div>
        <div class="rb-form-item">
            <a href="/profile/register"><?php echo $this->lang('register') ?></a> |
            <a href="/profile/forgot-password"><?php echo $this->lang('password_forgot') ?></a> |
            <a href="/"><?php echo $this->lang('home') ?></a>
            <?php echo $item->getFieldError('token') ?>
        </div>
    </form>
    <input type="hidden" class="js-launcher" value="profile" />
</div>
<div class="bg-white">
    <h2><?php echo $this->lang('register') ?></h2>
    <form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form" class="editor">
        <?php echo $item->buildId() ?>
        <?php echo $item->buildToken() ?>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('email') ?>
                <?php echo $item->req('email') ?>
                <?php echo $item->buildEmail() ?>
            </label>
            <?php echo $item->getFieldError('email') ?>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('password') ?>
                <?php echo $item->req('password') ?>
                <?php echo $item->buildPassword() ?>
            </label>
            <?php echo $item->getFieldError('password') ?>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo \Rebond\Services\Lang::lang('first_name') ?>
                <?php echo $item->req('firstName') ?>
                <?php echo $item->buildFirstName() ?>
            </label>
            <?php echo $item->getFieldError('firstName') ?>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo \Rebond\Services\Lang::lang('last_name') ?>
                <?php echo $item->req('lastName') ?>
                <?php echo $item->buildLastName() ?>
            </label>
            <?php echo $item->getFieldError('lastName') ?>
        </div>
        <div class="rb-form-item">
            <?php echo $this->buildSubmit(0, 'register') ?>
        </div>
        <div class="rb-form-item">
            <a href="/profile/sign-in"><?php echo $this->lang('sign_in') ?></a> |
            <a href="/"><?php echo $this->lang('home') ?></a>
            <?php echo $item->getFieldError('token') ?>
        </div>
    </form>
</div>
<input type="hidden" class="js-launcher" value="register" />

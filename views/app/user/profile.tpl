<div class="bg-white">
    <?php echo $this->renderTitle('profile', $item->getModel()->getId(), $item->getModel()) ?>
    <form class="editor" action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form" enctype="multipart/form-data">
        <?php echo $item->buildId() ?>
        <?php echo $item->buildToken() ?>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('first_name') ?>
                <?php echo $item->req('firstName') ?>
                <?php echo $item->buildFirstName() ?>
            </label>
            <?php echo $item->getFieldError('firstName') ?>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('last_name') ?>
                <?php echo $item->req('lastName') ?>
                <?php echo $item->buildLastName() ?>
            </label>
            <?php echo $item->getFieldError('lastName') ?>
        </div>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('avatar') ?>
                <?php echo $item->req('avatar') ?>
                <?php echo $item->buildAvatar(false) ?>
            </label>
            <?php echo $item->getFieldError('avatar') ?>
        </div>
        <div class="rb-form-item">
            <?php echo $this->buildSubmit($item->getModel()->getId()) ?>
            <a href="/profile" class="rb-btn rb-btn-blank"><?php echo $this->lang('cancel') ?></a>
            <?php echo $item->getFieldError('token') ?>
        </div>
    </form>
</div>
<input type="hidden" class="js-launcher" value="profile" />

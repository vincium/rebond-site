<div class="bg-white">
    <h2><?php echo $this->lang('password_forgot') ?></h2>
    <p><?php echo $this->lang('password_forgot_text') ?></p>
    <form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form" class="editor">
        <?php echo $item->buildId() ?>
        <?php echo $item->buildToken() ?>
        <?php echo $item->getFieldError('token') ?>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('email') ?>
                <?php echo $item->req('email') ?>
                <?php echo $item->buildEmail() ?>
            </label>
            <?php echo $item->getFieldError('email') ?>
        </div>
        <div class="rb-form-item">
            <?php echo $this->buildSubmit(0, 'send') ?>
        </div>
        <div class="rb-form-item">
            <a href="/profile/sign-in"><?php echo $this->lang('sign_in') ?></a> |
            <a href="/profile/register"><?php echo $this->lang('register') ?></a>
        </div>
    </form>
</div>

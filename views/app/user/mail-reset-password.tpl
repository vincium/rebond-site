<p><?php echo $this->lang('hi', [$item->getFullName()]) ?>,</p>
<p><?php echo $this->lang('password_reset_info') ?>:</p>
<p><a style="color:#336699;" href="<?php echo $url ?>profile/forgot-password/?reset=<?php echo $reset ?>"><?php echo $this->lang('password_reset_link') ?></a></p>

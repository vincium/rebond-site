<div class="bg-white">
    <h2><?php echo $this->lang($action) ?></h2>
    <form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST" name="form" id="form" class="editor">
        <?php echo $item->buildId() ?>
        <?php echo $item->buildToken() ?>
        <?php if ($checkCurrentPassword) { ?>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('password_current') ?>
                <?php echo $item->req('password') ?>
                <?php echo $item->buildPassword() ?>
            </label>
            <?php echo $item->getFieldError('password') ?>
        </div>
        <?php } ?>
        <div class="rb-form-item">
            <label>
                <?php echo $this->lang('password') ?>
                <?php echo $item->req('password') ?>
                <?php echo $item->buildPassword('new') ?>
            </label>
            <?php echo $item->getFieldError('passwordnew') ?>
        </div>
        <div class="rb-form-item">
            <?php echo $this->buildSubmit($item->getModel()->getId(), $action) ?>
            <a href="/"><?php echo $this->lang('cancel') ?></a>
            <?php echo $item->getFieldError('token') ?>
        </div>
    </form>
    <input type="hidden" class="js-launcher" value="changePassword" />
</div>
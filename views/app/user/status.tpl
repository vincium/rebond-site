<div class="bg-white">
    <?php if (isset($fullName)) { ?>
        <span>Hi <?php echo $fullName ?></span>,
        <form action="/profile/sign-out" method="POST" name="form" id="form">
            <a href="/profile/edit"><?php echo $this->lang('edit') ?></a> |
            <a href="/profile/change-password"><?php echo $this->lang('password_change') ?></a> |
            <input type="submit" class="rb-link" name="sign-out" value="<?php echo $this->lang('sign_out') ?>">
        </form>
    <?php } else { ?>
        <a href="/profile/sign-in"><?php echo $this->lang('sign_in') ?></a> |
        <a href="/profile/register"><?php echo $this->lang('register') ?></a> |
        <a href="/profile/forgot-password"><?php echo $this->lang('password_forgot') ?></a>
    <?php } ?>
</div>
<div class="bg-white">
    <h2><?php echo $this->lang('account_confirmed') ?></h2>
    <p><?php echo $this->lang('account_confirmed_text') ?></p>
    <a class="rb-btn" href="/profile"><?php echo $this->lang('profile_my') ?></a>
</div>
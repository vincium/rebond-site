<p><?php echo $this->lang('hi', [$item->getFullName()]) ?>,</p>
<p><?php echo $this->lang('register_confirmed', [$site]) ?></p>
<ul>
    <li>Email: <?php echo $item->getEmail() ?></li>
</ul>
<p><?php echo $this->lang('email_confirm_link') ?>:</p>
<p><a style="color:#336699;" href="<?php echo $url ?>profile/register/?confirm=<?php echo $confirm ?>"><?php echo $this->lang('email_confirm') ?></a></p>

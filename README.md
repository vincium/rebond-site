## What is Rebond?
**Rebond is a PHP framework packed with a code generator and a set of libraries to build web applications.**

#### Description
Rebond has an easy step by step installation which authenticates you, sets up the database,
creates an admin user and launch the site, either as a fully functional CMS or as a regular website
using an MVC architecture.
It runs in PHP5 or PHP7 (recommended)

#### Keywords
- Code generator
- CMS or MVC architecture
- Flexible
- Fast development
- High performance
- Lightweight
- Low learning curve
- Readable code
- Secure

#### Links
- https://www.facebook.com/rebond.framework
- https://vincium.blogspot.no/p/rebond.html

## Features
- Google analytics integration
- Intuitive user interface
- Logging tools
- User management
- Media management
- Wysiwyg
- Localization using yaml files
- Code generation from yaml files (models + model relationships, forms + validation, database access classes, html templates)
- Mail capabilities
- Up to date security requirements
- Javascript tools (notifications, modals, URL manipulation, improved password input, date input, localization)
- CSS and templates (*.tpl) editable in the administration site 
- Database backups and restore

#### CMS Features
- Page management
- Content Versioning
- Smart components
- Preview page before publishing content
- Custom modules capabilities

## Setup

#### Download
```
mkdir /work/my-site
git clone https://github.com/rebond/site.git my-site
cd my-site
composer install
cd www && npm install
cd ../admin && npm install
git remote set-url origin https://github.com/[your_git_repo]/[my-site].git
```
- Rename /files/config.yaml.dist to /files/config.yaml and edit the it to match your configuration
- Create an empty mysql database

#### Apache example
```
<VirtualHost *:80>
    DocumentRoot "/work/my-site/www"
    ServerName my-site.local
    SetEnv ENV "local"
    <Directory "/work/my-site/www">
        Require all granted
        AllowOverride All
    </Directory>
</VirtualHost>

<VirtualHost *:80>
    DocumentRoot "/work/my-site/admin"
    ServerName admin.my-site.local
    SetEnv ENV "local"
    <Directory "/work/wmy-site/admin">
        Require all granted
        AllowOverride All
    </Directory>
</VirtualHost>
```
- The rewrite_module module needs to be enabled.

#### Nginx example
###### Frontend config
```
server {
  listen 80;
  server_name my-site.local;
  root /work/my-site/www;
  index index.php;
  
  location / {
    try_files $uri $uri/ /index.php?$query_string;
  }
  
  location ~ \.php$ {
    include fastcgi_params;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    fastcgi_pass my-site:9000;
    fastcgi_param APP_ENV local;
  }
}
```

###### Backend config
```
server {
  listen 80;
  server_name admin.my-site.local;
  root /work/my-site/admin;
  index index.php;
  
  location / {
    try_files $uri $uri/ /index.php?$query_string;
  }
  
  location ~ \.php$ {
    include fastcgi_params;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    fastcgi_pass my-site:9000;
    fastcgi_param APP_ENV local;
  }
}
```

#### Host file
```
127.0.0.1 my-site.local
127.0.0.1 admin.my-site.local
```

## Installation
Go to http://admin.my-site.local and follow the 4 steps installation process:
1. Site authentication
2. Database installation
3. Admin user creation
4. Site launch
